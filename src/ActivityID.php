<?php
class ActivityID
{
    /**
     * @param $activityID
     * @return string
     */
    public static function toString($activityID=0)
    {
        $hotSpotStr = self::getHotSpotStr($activityID);
        $topicStr = self::getTopicStr($activityID);
        $levelStr = self::getLevelStr($activityID);

        return $levelStr . $topicStr . $hotSpotStr;
    }

    /**
     * @param $activityID
     * @return string
     */
    private static function getLevelStr($activityID)
    {
        return "L" . self::twoDigitStr(self::getLevelID($activityID));
    }

    /**
     * @param $activityID
     * @return float
     */
    private static function getLevelID($activityID)
    {
        return floor(($activityID -1) / 60) + 1;
    }

    /**
     * @param $activityID
     * @return string
     */
    private static function getTopicStr($activityID)
    {
        return "M" . self::twoDigitStr(self::getTopicID($activityID));
    }

    /**
     * @param $activityID
     * @return float
     */
    private static function getTopicID($activityID)
    {
        return (floor(($activityID - 1) / 3)) % 20 + 1;
    }

    /**
     * @param $activityID
     * @return string
     */
    private static function getHotSpotStr($activityID)
    {
        return self::twoDigitStr(self::getHotSpotID($activityID));
    }

    /**
     * @param $activityID
     * @return int
     */
    private static function getHotSpotID($activityID)
    {
        return (($activityID - 1) % 3) + 1;
    }

    /**
     * @param $number
     * @return string
     */
    private static function twoDigitStr($number)
    {
        return str_pad($number, 2, "0", STR_PAD_LEFT);
    }
}
