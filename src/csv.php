<?php

include "ActivityID.php";

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=ActivityID.csv');

$output = fopen('php://output', 'w');

fputcsv($output, array('activity id', 'activity string'));

for($i=1; $i<=960; $i++)
{
    fputcsv($output, array($i, ActivityID::toString($i)));
}