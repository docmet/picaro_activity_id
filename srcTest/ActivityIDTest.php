<?php

include "src/ActivityID.php";

class ActivityIDTest extends PHPUnit_Framework_TestCase
{
    public function testMethod_getActivityString_returnsString()
    {
        $this->assertInternalType("string", ActivityID::toString(), "Function does not return a string!");
    }

    public function testMethod_getActivityString_returnsRightStringForInt_1()
    {
        $this->assertEquals("L01M0101", ActivityID::toString(1));
    }

    public function testMethod_getActivityString_returnsRightStringForInt_2()
    {
        $this->assertEquals("L01M0102", ActivityID::toString(2));
    }

    public function testMethod_getActivityString_handlesHotSpotNumberCorrectlyForInt_4()
    {
        $actual = substr(ActivityID::toString(4), -1);
        $this->assertEquals("1", $actual);
    }

    public function testMethod_getActivityString_handlesHotSpotNumberCorrectlyFor_VariousIntegers()
    {
        $actual = substr(ActivityID::toString(3), -1);
        $this->assertEquals("3", $actual);

        $actual = substr(ActivityID::toString(5), -1);
        $this->assertEquals("2", $actual);

        $actual = substr(ActivityID::toString(6), -1);
        $this->assertEquals("3", $actual);

        $actual = substr(ActivityID::toString(11), -1);
        $this->assertEquals("2", $actual);

        $actual = substr(ActivityID::toString(12), -1);
        $this->assertEquals("3", $actual);
    }

    public function testMethod_getActivityString_handlesTopicNumberCorrectlyForInt_3()
    {
        $actual = ActivityID::toString(3);
        $this->assertEquals("L01M0103", $actual);
    }

    public function testMethod_getActivityString_handlesTopicNumberCorrectlyForInt_4()
    {
        $actual = ActivityID::toString(4);
        $this->assertEquals("L01M0201", $actual);
    }

    public function testMethod_getActivityString_handlesTopicNumberCorrectlyForInt_28()
    {
        $actual = ActivityID::toString(28);
        $this->assertEquals("L01M1001", $actual);
    }

    public function testMethod_getActivityString_handlesTopicNumberCorrectlyForInt_61()
    {
        $actual = substr(ActivityID::toString(61), 3);
        $this->assertEquals("M0101", $actual);
    }

    public function testMethod_getActivityString_handlesLevelNumberCorrectlyForInt_60()
    {
        $actual = ActivityID::toString(60);
        $this->assertEquals("L01M2003", $actual);
    }

    public function testMethod_getActivityString_handlesLevelNumberCorrectlyForInt_61()
    {
        $actual = ActivityID::toString(61);
        $this->assertEquals("L02M0101", $actual);
    }

    public function testMethod_getActivityString_handlesLevelNumberCorrectlyForInt_541()
    {
        $actual = ActivityID::toString(541);
        $this->assertEquals("L10M0101", $actual);
    }
}